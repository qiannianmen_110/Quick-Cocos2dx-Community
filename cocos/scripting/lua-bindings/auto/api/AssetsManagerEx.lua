
--------------------------------
-- @module AssetsManagerEx
-- @extend Ref
-- @parent_module cc

--------------------------------
--  @brief Gets the current update state.
-- @function [parent=#AssetsManagerEx] getState 
-- @param self
-- @return int#int ret (return value: int)
        
--------------------------------
--  @brief  Check out if there is a new version of manifest.<br>
-- You may use this method before updating, then let user determine whether<br>
-- he wants to update resources.
-- @function [parent=#AssetsManagerEx] checkUpdate 
-- @param self
        
--------------------------------
--  @brief Gets storage path.
-- @function [parent=#AssetsManagerEx] getStoragePath 
-- @param self
-- @return string#string ret (return value: string)
        
--------------------------------
--  @brief Update with the current local manifest.
-- @function [parent=#AssetsManagerEx] update 
-- @param self
        
--------------------------------
--  @brief Function for retrieve the local manifest object
-- @function [parent=#AssetsManagerEx] getLocalManifest 
-- @param self
-- @return Manifest#Manifest ret (return value: cc.Manifest)
        
--------------------------------
--  @brief Function for retrieve the remote manifest object
-- @function [parent=#AssetsManagerEx] getRemoteManifest 
-- @param self
-- @return Manifest#Manifest ret (return value: cc.Manifest)
        
--------------------------------
--  @brief Reupdate all failed assets under the current AssetsManagerEx context
-- @function [parent=#AssetsManagerEx] downloadFailedAssets 
-- @param self
        
--------------------------------
-- @overload self, string, string, string, string, string, string         
-- @overload self, string, string, string, string, string         
-- @function [parent=#AssetsManagerEx] create
-- @param self
-- @param #string manifestUrl
-- @param #string storagePath
-- @param #string versionFileName
-- @param #string projectFileName
-- @param #string tempProjectFileName
-- @param #string versionFileURL
-- @return AssetsManagerEx#AssetsManagerEx ret (return value: cc.AssetsManagerEx)

return nil
